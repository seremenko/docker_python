FROM tensorflow/tensorflow

RUN apt-get update
RUN apt-get install -y musl-dev wget git

RUN wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz && \
  tar -xvzf ta-lib-0.4.0-src.tar.gz && \
  cd ta-lib/ && \
  ./configure --prefix=/usr && \
  make && \
  make install
RUN git clone https://github.com/mrjbq7/ta-lib.git /ta-lib-py && cd /ta-lib-py && python setup.py install

RUN pip install keras

